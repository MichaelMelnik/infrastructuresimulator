package itmo.escience.simenv.algorithms.pso

import java.util
import java.util.Random

import itmo.escience.simenv.algorithms.ga.{WorkflowSchedulingProblem, WFSchedSolution, ScheduleFitnessEvaluator}
import itmo.escience.simenv.algorithms.{HEFTScheduler, MinMinScheduler, Scheduler}
import itmo.escience.simenv.environment.entities._
import itmo.escience.simenv.environment.modelling.Environment
import org.uncommons.maths.random.MersenneTwisterRNG
import org.uncommons.watchmaker.framework._
import org.uncommons.watchmaker.framework.operators.EvolutionPipeline
import org.uncommons.watchmaker.framework.selection.RouletteWheelSelection
import org.uncommons.watchmaker.framework.termination.GenerationCount


// w - inertia; c1 - personal best attraction; c2 - global best attraction; buddies - neighbors
class PSOScheduler(w: Double, c1: Double, c2: Double, buddies: Int,
                   popSize:Int, iterationCount: Int) extends Scheduler{



  override def schedule[T <: Task, N <: Node](context: Context[T, N], environment: Environment[N]): Schedule[T, N] = {
    val idMaps = PsoOperators.constructIdMaps[T, N](context, environment)
    val taskIdMap = idMaps._1
    val nodeIdMap = idMaps._2

    val factory: PsoScheduleCandidateFactory[T, N] = new PsoScheduleCandidateFactory[T, N](context, environment, taskIdMap, nodeIdMap)

    val operators: util.List[EvolutionaryOperator[PsoWFSchedSolution]] = new util.LinkedList[EvolutionaryOperator[PsoWFSchedSolution]]()
    operators.add(new PsoUpdateOperator(w, c1, c2, buddies))

    val pipeline: EvolutionaryOperator[PsoWFSchedSolution] = new EvolutionPipeline[PsoWFSchedSolution](operators)

    val fitnessEvaluator: FitnessEvaluator[WFSchedSolution] = new ScheduleFitnessEvaluator[T, N](context, environment)

    val rng: Random = new MersenneTwisterRNG()

    val  engine: PsoAlgorithm[T, N] = new PsoAlgorithm[T, N](factory,
      pipeline,
      fitnessEvaluator,
      rng, popSize)

    engine.addEvolutionObserver(new EvolutionObserver[PsoWFSchedSolution]()
    {
      def populationUpdate(data :PopulationData[_ <: PsoWFSchedSolution]) =
      {
        val best = data.getBestCandidate
        val bestMakespan = WorkflowSchedulingProblem.solutionToSchedule(best, context, environment).makespan()
        println(s"Generation ${data.getGenerationNumber}: $bestMakespan\n")
      }
    })

    val heft_schedule = HEFTScheduler.schedule(context, environment)
    val seeds: util.ArrayList[PsoWFSchedSolution] = new util.ArrayList[PsoWFSchedSolution]()
    val heft_sol = WorkflowSchedulingProblem.scheduleToSolution[T, N](heft_schedule, context, environment)
    val adapted_heft = PsoOperators.adaptSolution(heft_sol, taskIdMap, nodeIdMap)
//    seeds.add(adapted_heft)

    val algResult = engine.evolve(popSize, 1, seeds, new GenerationCount(iterationCount))
    val result = algResult
//    val details = algResult._2
    WorkflowSchedulingProblem.solutionToSchedule(result, context, environment)
  }

}
