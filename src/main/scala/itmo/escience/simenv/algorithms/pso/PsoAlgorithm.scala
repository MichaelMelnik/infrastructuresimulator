package itmo.escience.simenv.algorithms.pso

import java.util
import java.util.Random

import itmo.escience.simenv.algorithms.ga.WFSchedSolution
import itmo.escience.simenv.environment.entities.{Node, Task}
import org.uncommons.watchmaker.framework._

import scala.collection.JavaConversions._

/**
  * Created by mikhail on 02.02.2016.
  */
class PsoAlgorithm[T <: Task, N <: Node](factory: PsoScheduleCandidateFactory[T, N],
                                         pipeline: EvolutionaryOperator[PsoWFSchedSolution],
                                         fitnessEvaluator: FitnessEvaluator[WFSchedSolution],
                                         rng: Random, popSize: Int) extends GenerationalEvolutionEngine[PsoWFSchedSolution](factory,
                                                                           pipeline,
                                                                           fitnessEvaluator,
                                                                           null,
                                                                           rng) {

  def evolve(populationSize: Int, eliteCount: Int, seedCandidates: util.Collection[PsoWFSchedSolution], conditions: TerminationCondition): PsoWFSchedSolution = {
    val res = evolvePopulation(populationSize, eliteCount, seedCandidates, conditions)
    res.get(0).getCandidate
  }


  private var _fitnesses: Int = 0
  private var _globBest: PsoWFSchedSolution = null

  def evolvePopulation(populationSize: Int, eliteCount: Int, seedCandidates: util.Collection[PsoWFSchedSolution], conditions: TerminationCondition): util.List[EvaluatedCandidate[PsoWFSchedSolution]] = {
    _fitnesses = 0
    if(eliteCount >= 0 && eliteCount < populationSize) {
      if(conditions == null) {
        throw new IllegalArgumentException("At least one TerminationCondition must be specified.")
      } else {
        var currentGenerationIndex: Int = 0
        val startTime: Long = System.currentTimeMillis()
        val population = factory.generateInitialPopulation(populationSize, seedCandidates, rng)
        var evaluatedPopulation = evaluate(population)
        EvolutionUtils.sortEvaluatedPopulation(evaluatedPopulation, fitnessEvaluator.isNatural)
        var data: PopulationData[PsoWFSchedSolution] = EvolutionUtils.getPopulationData(evaluatedPopulation, fitnessEvaluator.isNatural, eliteCount, currentGenerationIndex, startTime)
        val best = data.getBestCandidate

        _globBest = best.copy

        var timePing: Long = System.currentTimeMillis()
        println(s"Generation ${data.getGenerationNumber}: ${_globBest.fitness}")

        while(!conditions.shouldTerminate(data)) {
          currentGenerationIndex += 1
          evaluatedPopulation = nextEvolutionStep(evaluatedPopulation, eliteCount, rng)
          data = EvolutionUtils.getPopulationData(evaluatedPopulation, fitnessEvaluator.isNatural, eliteCount, currentGenerationIndex, startTime)

          val best = data.getBestCandidate
          if (best.fitness < _globBest.fitness) {
            _globBest = best.copy
          }

          timePing = System.currentTimeMillis()
          println(s"Generation ${data.getGenerationNumber}: ${_globBest.fitness}")
        }
        evaluatedPopulation.add(new EvaluatedCandidate[PsoWFSchedSolution](_globBest, _globBest.fitness))
        EvolutionUtils.sortEvaluatedPopulation(evaluatedPopulation, fitnessEvaluator.isNatural)
        evaluatedPopulation
      }
    } else {
      throw new IllegalArgumentException("Elite count must be non-negative and less than population size.")
    }
  }


  override def nextEvolutionStep(evaluatedPopulation: util.List[EvaluatedCandidate[PsoWFSchedSolution]], eliteCount: Int, rng: Random): util.List[EvaluatedCandidate[PsoWFSchedSolution]] = {
    val population :util.ArrayList[PsoWFSchedSolution] = new util.ArrayList[PsoWFSchedSolution](evaluatedPopulation.size())
    for (c <- evaluatedPopulation) {
      population.add(c.getCandidate)
    }
    population.add(_globBest.copy)
    val updatedPop: util.List[PsoWFSchedSolution] = pipeline.apply(population, rng)

    if (evaluatedPopulation.size != updatedPop.size) {
      throw new IllegalStateException("Population size is not correct")
    }

    val evPop = evaluate(updatedPop)
    // update personal best
    for (c <- evPop) {
      val candid = c.getCandidate
      val curFit = c.getFitness
      val pFit = candid.bestFit
      if (curFit <= pFit) {
        candid.setMapBest(candid.mapPos)
        candid.setOrdBest(candid.ordPos)
        candid.setBestFit(curFit)
      }
    }
    EvolutionUtils.sortEvaluatedPopulation(evPop, fitnessEvaluator.isNatural)
    evPop
  }

  def evaluate(pop: util.List[PsoWFSchedSolution]): util.List[EvaluatedCandidate[PsoWFSchedSolution]] = {
    _fitnesses += pop.size()
    val res = new util.ArrayList[EvaluatedCandidate[PsoWFSchedSolution]]()
    val parallel = pop.par.map(p => {
      val fit = fitnessEvaluator.getFitness(p, null)
      p.fitness = fit
      new EvaluatedCandidate[PsoWFSchedSolution](p, fit)
    }
    )
    for (p <- parallel.toList) {
      res.add(p)
    }
    res
  }
}
