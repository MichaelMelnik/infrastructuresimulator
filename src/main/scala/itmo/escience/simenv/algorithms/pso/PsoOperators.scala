package itmo.escience.simenv.algorithms.pso

import java.util

import itmo.escience.simenv.algorithms.ga.WFSchedSolution
import itmo.escience.simenv.environment.entities.{Node, Task, Context}
import itmo.escience.simenv.environment.modelling.Environment
import Array._

/**
  * Created by mikhail on 17.08.2016.
  */
object PsoOperators {

  def constructIdMaps[T <: Task, N <: Node](context: Context[T, N], environment: Environment[N]): (util.HashMap[Int, String], util.HashMap[Int, String]) = {
    val nodes = environment.nodes.map(x => x.id)
    val nodeIdMap = new util.HashMap[Int, String]()
    for (i <- nodes.indices) {
      nodeIdMap.put(i, nodes(i))
    }
    val tasks = context.workload.apps.foldLeft(List[Task]())((s, x) => s ++ x.tasks).map(t => t.id).distinct
    val taskIdMap = new util.HashMap[Int, String]()
    for (i <- tasks.indices) {
      taskIdMap.put(i, tasks(i))
    }
    (taskIdMap, nodeIdMap)
  }

  def adaptSolution[T, N](solution: WFSchedSolution, taskIdMap: util.HashMap[Int, String], nodeIdMap: util.HashMap[Int, String]): PsoWFSchedSolution = {
    new PsoWFSchedSolution(solution.genSeq, taskIdMap, nodeIdMap)
  }

  // Mapping

  // Subtraction: p1.map - p2.map
  def mapSubtraction(p1: PsoWFSchedSolution, p2: PsoWFSchedSolution): Array[Array[Int]] = {
    val m = p1.taskSize
    val n = p1.nodeSize
    val vel = ofDim[Int](m, n)
    for (i <- 0 until m) {
      for (j <- 0 until n) {
        vel(i)(j) = p1.mapPos(i)(j) - p2.mapPos(i)(j)
      }
    }
    vel
  }

  // pbest - x
  def mapSubtractionSelf(p: PsoWFSchedSolution): Array[Array[Int]] = {
    val m = p.taskSize
    val n = p.nodeSize
    val vel = ofDim[Int](m, n)
    for (i <- 0 until m) {
      for (j <- 0 until n) {
        vel(i)(j) = p.mapBest(i)(j) - p.mapPos(i)(j)
      }
    }
    vel
  }

  def ordSubtraction(p1: PsoWFSchedSolution, p2: PsoWFSchedSolution): Array[Int] = {
    val m = p1.taskSize
    val dist = new Array[Int](m)
    for (i <- 0 until m) {
      dist(i) = p1.ordPos(i) - p2.ordPos(i)
    }
    dist
  }

  def ordSubtractionSelf(p: PsoWFSchedSolution): Array[Int] = {
    val m = p.taskSize
    val dist = new Array[Int](m)
    for (i <- 0 until m) {
      dist(i) = p.ordBest(i) - p.ordPos(i)
    }
    dist
  }

}
