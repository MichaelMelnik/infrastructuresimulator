package itmo.escience.simenv.algorithms.pso

import java.util
import java.util.Random

import itmo.escience.simenv.algorithms.RandomScheduler
import itmo.escience.simenv.algorithms.ga.WorkflowSchedulingProblem
import itmo.escience.simenv.environment.entities._
import itmo.escience.simenv.environment.modelling.Environment
import org.uncommons.watchmaker.framework.factories.AbstractCandidateFactory

/**
  * Created by mikhail on 22.01.2016.
  */
class PsoScheduleCandidateFactory[T <: Task, N <: Node](ctx: Context[T, N], env: Environment[N], taskIdMap: util.HashMap[Int, String], nodeIdMap: util.HashMap[Int, String]) extends AbstractCandidateFactory[PsoWFSchedSolution]{

  override def generateRandomCandidate(random: Random): PsoWFSchedSolution = {
    val schedule = RandomScheduler.schedule[T, N](ctx, env)
    val solution = WorkflowSchedulingProblem.scheduleToSolution[T, N](schedule, ctx, env)
    val res = new PsoWFSchedSolution(solution.genSeq, taskIdMap, nodeIdMap)
    res
  }
}
