package itmo.escience.simenv.algorithms.pso

import java.util

import itmo.escience.simenv.algorithms.ga.{WFSchedSolution, EvSolution, MappedTask}

import scala.collection.JavaConversions._
import Array._

/**
 * individual for genetic algorithm
 */
class PsoWFSchedSolution(mappedTasks: List[MappedTask], taskIdMap: java.util.HashMap[Int, String], nodeIdMap: java.util.HashMap[Int, String]) extends WFSchedSolution(mappedTasks) {

//  def this(that:PsoWFSchedSolution) = this(that._genes.toList)

  val taskSize = taskIdMap.size()
  val nodeSize = nodeIdMap.size()

  private var _genes = new util.ArrayList(mappedTasks)

  private var _mapPos = ofDim[Int](taskSize, nodeSize)
  private var _mapVel = ofDim[Double](taskSize, nodeSize)
  private var _mapBest = ofDim[Int](taskSize, nodeSize)
  private var _ordPos = new Array[Int](taskSize)
  private var _ordVel = new Array[Double](taskSize)
  private var _ordBest = new Array[Int](taskSize)
  private var _bestFit: Double = 66613666
  initialization()

  def mapPos = _mapPos
  def mapVel = _mapVel
  def mapBest = _mapBest
  def ordPos = _ordPos
  def ordVel = _ordVel
  def ordBest = _ordBest
  def bestFit = _bestFit

  def setMapVel(newVel: Array[Array[Double]]) = {
    _mapVel = newVel
  }
  def setMapBest(newBest: Array[Array[Int]]) = {
    _mapBest = newBest
  }
  def setMapPos(newPos: Array[Array[Int]]) = {
    _mapPos = newPos
  }

  def setOrdVel(newVel:Array[Double]) = {
    _ordVel = newVel
  }
  def setOrdBest(newBest: Array[Int]) = {
    _ordBest = newBest
  }
  def setOrdPos(newPos: Array[Int]) = {
    _ordPos = newPos
  }

  def setBestFit(newFit: Double) = {
    _bestFit = newFit
  }

  def updateGenes() = {
    val newGenes = new util.ArrayList[MappedTask]()
    for (i <- 0 until taskSize) {
      val taskIdx = _ordPos.indexOf(i)
      val task = taskIdMap.get(taskIdx)
      val nodeIdx = _mapPos(taskIdx).indexOf(1)
      val node = nodeIdMap.get(nodeIdx)
      newGenes.add(new MappedTask(task, node))
    }
    _genes = newGenes
  }

  def initialization() = {
    for (i <- 0 until taskSize) {
      for (j <- 0 until nodeSize) {
        _mapPos(i)(j) = 0
        _mapVel(i)(j) = 0
      }
      _ordPos(i) = 0
      _ordVel(i) = 0
    }
    for (ord <- mappedTasks.indices) {
      val item = mappedTasks(ord)
      val task = item.taskId
      val node = item.nodeId
      val taskIdx = taskIdMap.filter(x => x._2 == task).head._1
      val nodeIdx = nodeIdMap.filter(x => x._2 == node).head._1
      _mapPos(taskIdx)(nodeIdx) = 1
      _ordPos(taskIdx) = ord
    }
    _mapBest = _mapPos
    _ordBest = _ordPos
  }

  def setGenes(sol: PsoWFSchedSolution) = {
    _genes = new util.ArrayList[MappedTask](sol.genSeq)
    fitness = sol.fitness
  }

  override def setVariableValue(i: Int, t: MappedTask): Unit = {
    _genes.set(i, t)
  }

  override def getVariableValue(i: Int): MappedTask = {
    _genes.get(i)
  }

  override def getVariableValueString(i: Int): String = {
    throw new NotImplementedError()
  }

  override def copy: PsoWFSchedSolution = {
    val res = new PsoWFSchedSolution(this.genSeq, taskIdMap, nodeIdMap)
    res.fitness = this.fitness
    res._mapBest = this._mapBest
    res._mapPos = this._mapPos
    res._mapVel = this._mapVel
    res._ordBest = this._ordBest
    res._ordPos = this._ordPos
    res._ordVel = this._ordVel
    res
  }

  override def getNumberOfVariables: Int = _genes.size()

  override def genSeq: List[MappedTask] = _genes.toList
}
