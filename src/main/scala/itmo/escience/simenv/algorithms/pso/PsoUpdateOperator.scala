package itmo.escience.simenv.algorithms.pso

import java.util.Random
import java.util

import org.uncommons.watchmaker.framework.EvolutionaryOperator
import collection.JavaConversions._
import Array._

/**
  * Created by mikhail on 17.08.2016.
  */
class PsoUpdateOperator (w: Double, c1: Double, c2: Double, buddies: Int) extends EvolutionaryOperator[PsoWFSchedSolution] {

  override def apply(pop: util.List[PsoWFSchedSolution], random: Random): util.List[PsoWFSchedSolution] = {
    val globBest = pop(pop.size - 1)
    pop.remove(pop.size - 1)
    val len = pop.size

    val newPop: util.ArrayList[PsoWFSchedSolution] = new util.ArrayList[PsoWFSchedSolution](len)
    for (p <- pop) {
      newPop.add(p.copy)
    }

    // Mapping
    mappingUpdate(pop, newPop, globBest, random)
    // Ordering
    orderingUpdate(pop, newPop, globBest, random)
    // Update genes according to new mapping and ordering
    genesUpdate(newPop)

    newPop
  }

  def mappingUpdate(pop: util.List[PsoWFSchedSolution], newPop: util.List[PsoWFSchedSolution], globBest: PsoWFSchedSolution, rnd: Random) = {
    val m = globBest.taskSize
    val n = globBest.nodeSize
    val len = pop.size()

    // Distance matrix
    val distances = ofDim[Double](len, len + 1)
    for (i <- pop.indices) {
      val p1 = pop(i)
      for (j <- i until (len + 1)) {
        if (i == j) {
          distances(i)(j) = -666
        } else {
          var p2: PsoWFSchedSolution = null
          if (j == len) {
            p2 = globBest
          } else {
            p2 = pop(j)
          }
          val distVec = PsoOperators.mapSubtraction(p2, p1)
          var dist = 0.0
          for (r <- 0 until m) {
            for (c <- 0 until n) {
              dist += math.pow(distVec(r)(c), 2)
            }
          }
          dist = math.sqrt(dist)
          distances(i)(j) = dist
          if (j < len) {
            distances(j)(i) = dist
          }
        }
      }
    }

    // map update of particles
    for (i <- newPop.indices) {
      val p = newPop(i)
      // select nearest buddies
      val pRow = distances(i)
      val pDist = pRow.indices.map(x => (x, pRow(x))).filter(x => x._2 >= 0).sortBy(x => x._2).take(buddies)
      var buddyList = List[PsoWFSchedSolution]()
      for (it <- pDist) {
        val idx = it._1
        if (idx == len) {
          buddyList :+= globBest
        } else {
          buddyList :+= pop(idx)
        }
      }
      val bestBuddy = buddyList.sortBy(x => x.fitness).head
      // Particle update
      // v_t+1 = w*v_t + c1*(pbest - x_t)*r1 + c2*(gbest - x_t)*r2
      val dist2p = PsoOperators.mapSubtractionSelf(p)
      val dist2g = PsoOperators.mapSubtraction(bestBuddy, p)
      val newVel = ofDim[Double](m, n)
      val r1 = rnd.nextDouble()
      val r2 = rnd.nextDouble()
      for (j <- 0 until m) {
        for (k <- 0 until n) {
          newVel(j)(k) = w * p.mapVel(j)(k) + r1 * c1 * dist2p(j)(k) + r2 * c2 * dist2g(j)(k)
        }
      }
      p.setMapVel(newVel)
      // TODO normalization of velocity

      // x_t+1 = x_t + v_t+1
      val tempNewPos = ofDim[Double](m, n)
      for (j <- 0 until m) {
        for (k <- 0 until n) {
          tempNewPos(j)(k) = p.mapPos(j)(k) + p.mapVel(j)(k)
        }
      }
      val newPos = ofDim[Int](m, n)
      for (j <- 0 until m) {
        for (k <- 0 until n) {
          newPos(j)(k) = 0
        }
      }
      for (j <- 0 until m) {
        // convert
        val row = tempNewPos(j)
        val maxValue = row.max
        val maxIndexes = row.indices.filter(x => row(x) == maxValue)
        val bestIdx = maxIndexes(rnd.nextInt(maxIndexes.size))
        newPos(j)(bestIdx) = 1
      }
      p.setMapPos(newPos)
    }
  }

  def orderingUpdate(pop: util.List[PsoWFSchedSolution], newPop: util.List[PsoWFSchedSolution], globBest: PsoWFSchedSolution, rnd: Random) = {
    val m = globBest.taskSize
    val len = pop.size()

    // Distance matrix
    val distances = ofDim[Double](len, len + 1)
    for (i <- pop.indices) {
      val p1 = pop(i)
      for (j <- i until (len + 1)) {
        if (i == j) {
          distances(i)(j) = -666
        } else {
          var p2: PsoWFSchedSolution = null
          if (j == len) {
            p2 = globBest
          } else {
            p2 = pop(j)
          }
          val distVec = PsoOperators.ordSubtraction(p2, p1)
          var dist = 0.0
          for (r <- 0 until m) {
              dist += math.pow(distVec(r), 2)
          }
          dist = math.sqrt(dist)
          if (dist.isNaN) {
            println("Nu che zz nahooy?")
          }
          distances(i)(j) = dist
          if (j < len) {
            distances(j)(i) = dist
          }

        }
      }
    }
    // ord update of particles
    for (i <- newPop.indices) {
      val p = newPop(i)
      // select nearest buddies
      val pRow = distances(i)
      val pDist = pRow.indices.map(x => (x, pRow(x))).filter(x => x._2 >= 0).sortBy(x => x._2).take(buddies)
      var buddyList = List[PsoWFSchedSolution]()
      for (it <- pDist) {
        val idx = it._1
        if (idx == len) {
          buddyList :+= globBest
        } else {
          buddyList :+= pop(idx)
        }
      }
      if (buddyList.isEmpty) {
        println("Che za nahooy?")
      }
      val bestBuddy = buddyList.sortBy(x => x.fitness).head
      // Particle update
      // v_t+1 = w*v_t + c1*(pbest - x_t)*r1 + c2*(gbest - x_t)*r2
      val dist2p = PsoOperators.ordSubtractionSelf(p)
      val dist2g = PsoOperators.ordSubtraction(bestBuddy, p)
      val newVel = new Array[Double](m)
      val r1 = rnd.nextDouble()
      val r2 = rnd.nextDouble()
      for (j <- 0 until m) {
        newVel(j) = w * p.ordVel(j) + r1 * c1 * dist2p(j) + r2 * c2 * dist2g(j)
      }
      p.setOrdVel(newVel)

      // x_t+1 = x_t + v_t+1
      val tempNewPos = new Array[Double](m)
      for (j <- 0 until m) {
        tempNewPos(j) = p.ordPos(j) + p.ordVel(j)
      }
      val newPos = new Array[Int](m)
      for (j <- 0 until m) {
        newPos(j) = 0
      }
      // convert
      val indices = tempNewPos.indices
      val idValues = indices.map(x => (x, tempNewPos(x)))
      val sortedValues = idValues.sortBy(x => x._2)
      for (j <- sortedValues.indices) {
        val item = sortedValues(j)
        newPos(item._1) = j
      }
      p.setOrdPos(newPos)
    }
  }

  def genesUpdate(pop: util.List[PsoWFSchedSolution]) = {
    for (p <- pop) {
      p.updateGenes()
    }
  }

}