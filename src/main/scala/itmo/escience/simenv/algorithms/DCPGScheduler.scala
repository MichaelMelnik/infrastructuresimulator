package itmo.escience.simenv.algorithms

import java.util
import java.util.Random

import itmo.escience.simenv.environment.entities._
import itmo.escience.simenv.environment.entitiesimpl.{CarrierNodeEnvironment, SingleAppWorkload}
import itmo.escience.simenv.environment.modelling.Environment

import scala.collection.JavaConversions._

/**
 * Created by Nikolay on 12/1/2015.
 */
object DCPGScheduler extends Scheduler{

  override def schedule[T <: Task, N <: Node](context: Context[T, N], environment: Environment[N]): Schedule[T, N] = {
    val adaptContext = context.asInstanceOf[Context[DaxTask, CapacityBasedNode]]
    val adaptEnv = environment.asInstanceOf[CarrierNodeEnvironment[CapacityBasedNode]]
    adaptSchedule(adaptContext, adaptEnv).asInstanceOf[Schedule[T, N]]
  }

  def adaptSchedule(context: Context[DaxTask, CapacityBasedNode], environment: Environment[CapacityBasedNode]): Schedule[DaxTask, CapacityBasedNode] = {

    val rnd = new Random()

    val nodes = environment.nodes
    val maxCap = nodes.map(x => x.capacity).max

    val bandwidths = environment.networks
    val maxBand = bandwidths.map(x => x.bandwidth).max

    val wf = context.workload.asInstanceOf[SingleAppWorkload].app
    val tasks = wf.tasks.asInstanceOf[Seq[DaxTask]].distinct
    val newSchedule = context.schedule.fixedSchedule()
    val fixedTasks = newSchedule.scheduleItemsSeq().filter(x=> x.status != ScheduleItemStatus.FAILED).map(x=> x.asInstanceOf[TaskScheduleItem[DaxTask, CapacityBasedNode]].task.id)
    val scheduledTasks: util.HashMap[String, String] = new util.HashMap[String, String]()
    var scheduleOrder = List[(String, String)]()
    var notScheduledTasks = List[String]()
    for (t <- tasks) {
      if (!fixedTasks.contains(t.id)) {
        notScheduledTasks :+= t.id
      } else {
        val lastItem = newSchedule.lastTaskItem(t.id)
        val itemNode = newSchedule.getMap.keySet().toList.filter(x => newSchedule.getMap.get(x).toList.map(y=>y.id).contains(lastItem.id)).head
        scheduledTasks.put(t.id, itemNode)
      }
    }

    val aetMap = new util.HashMap[String, Double]()
    val adttMap = new util.HashMap[String, Double]()
    var aestMap: util.HashMap[String, Double] = null
    var alstMap: util.HashMap[String, Double] = null
    var dcplVal: Double = -Double.MaxValue

//    var aestMap: util.HashMap[String, Double] = null

    // util functions
    def aet(t: DaxTask): Double = {
      if (aetMap.containsKey(t.id)) {
        return aetMap.get(t.id)
      }
      t.execTime / maxCap
    }
    def adtt(t: DaxTask): Double = {
      if (adttMap.containsKey(t.id)) {
        return adttMap.get(t.id)
      }
      t.outputData.map(x => x.volume).sum / maxBand
    }

    def aest(t: DaxTask, r: CapacityBasedNode): Double = {
      if (aestMap.containsKey(t.id)) {
        return aestMap.get(t.id)
      }
      val parents = t.parents
      if (parents.isEmpty || (parents.size == 1 && parents.head.isInstanceOf[HeadDaxTask])) {
        return 0.0
      }
      val maxParValue = parents.map(p => {
        var result = aet(p)
        if (scheduledTasks.containsKey(p.id)) {
          val rpId = scheduledTasks.get(p.id)
          val rp = environment.nodeById(rpId).asInstanceOf[CapacityBasedNode]
          result += aest(p, rp)
          if (r.id != rp.id || !scheduledTasks.containsKey(t.id)) {
            result += adtt(p)
          }
        } else {
          result += aest(p, r)
          result += adtt(p)
        }
        return result
      }).max
      maxParValue
    }

    def dcpl(): Double = {
      val result = tasks.map(t => {
        var curRes = aet(t)
        var r: CapacityBasedNode = null
        if (scheduledTasks.containsKey(t.id)) {
          val rId = scheduledTasks.get(t.id)
          r = environment.nodeById(rId).asInstanceOf[CapacityBasedNode]
        } else {
          r = nodes(rnd.nextInt(nodes.size))
        }
        curRes += aest(t, r)
        return curRes
      }).max
      result
    }

    def alst(t: DaxTask, r: CapacityBasedNode): Double = {
      if (alstMap.containsKey(t.id)) {
        return alstMap.get(t.id)
      }
      val children = t.children
      if (children.isEmpty) {
        return dcpl() - aet(t)
      }
      val minChildVal = children.map(c => {
        var curRes = -aet(t)

        if (scheduledTasks.containsKey(c.id)) {
          val rpId = scheduledTasks.get(c.id)
          val rp = environment.nodeById(rpId).asInstanceOf[CapacityBasedNode]
          curRes += alst(c, rp)
          if (r.id != rp.id || !scheduledTasks.containsKey(t.id)) {
            curRes -= adtt(c)
          }
        } else {
          curRes += alst(c, r)
          curRes -= adtt(c)
        }
        curRes
      }).min
      minChildVal
    }

    // Alg
    for (t <- tasks) {
      aetMap.put(t.id, aet(t))
      adttMap.put(t.id, adtt(t))
    }

    // aest
    aestMap = new util.HashMap[String, Double]()
    var readyTasks = tasks.filter(x => x.parents.size == 1 && x.parents.head.isInstanceOf[HeadDaxTask])
    var evaluatedTasks = List[DaxTask]()
    while (readyTasks.nonEmpty) {
      for (t <- readyTasks) {
        var aestNode: CapacityBasedNode = null
        if (scheduledTasks.containsKey(t.id)) {
          aestNode = environment.nodeById(scheduledTasks.get(t.id)).asInstanceOf[CapacityBasedNode]
        } else {
          aestNode = nodes(rnd.nextInt(nodes.size))
        }
        val aestVal = aest(t, aestNode)
        evaluatedTasks :+= t
        aestMap.put(t.id, aestVal)
      }
      // TODO check headTasks
      readyTasks = List[DaxTask]()
      for (t <- tasks) {
        var isReady = true
        if (evaluatedTasks.contains(t)) {
          isReady = false
        } else {
          val tparents = t.parents
          for (par <- tparents) {
            if (!readyTasks.contains(par)) {
              isReady = false
            }
          }
        }
        if (isReady) {
          readyTasks :+= t
        }
      }
    }

    // dcpl
    dcplVal = dcpl()

    // alst
    alstMap = new util.HashMap[String, Double]()
    var alstReadyTasks = tasks.filter(x => x.children.isEmpty)
    var alstEvaluatedTasks = List[DaxTask]()
    while (alstReadyTasks.nonEmpty) {
      for (t <- alstReadyTasks) {
        var alstNode: CapacityBasedNode = null
        if (scheduledTasks.containsKey(t.id)) {
          alstNode = environment.nodeById(scheduledTasks.get(t.id)).asInstanceOf[CapacityBasedNode]
        } else {
          alstNode = nodes(rnd.nextInt(nodes.size))
        }
        val alstVal = alst(t, alstNode)
        alstEvaluatedTasks :+= t
        aestMap.put(t.id, alstVal)
      }
      // TODO check headTasks
      alstReadyTasks = List[DaxTask]()
      for (t <- tasks) {
        var isReady = true
        if (alstEvaluatedTasks.contains(t)) {
          isReady = false
        } else {
          val tchildren = t.children
          for (child <- tchildren) {
            if (!alstReadyTasks.contains(child)) {
              isReady = false
            }
          }
        }
        if (isReady) {
          alstReadyTasks :+= t
        }
      }
    }

    while (notScheduledTasks.nonEmpty) {

      var readyUnsTasks = List[String]()
      for (tid <- notScheduledTasks) {
        val task = wf.taskById(tid)
        val tparents = task.parents
        var isReadyT = true
        for (p <- tparents) {
          if (!p.isInstanceOf[HeadDaxTask] && !scheduledTasks.containsKey(p.id)) {
            isReadyT = false
          }
        }
        if (isReadyT) {
          readyUnsTasks :+= task.id
        }
      }

      // schedule tasks
      while (readyUnsTasks.nonEmpty) {
        val minAestTaskId = readyUnsTasks.map(x => (x, math.abs(aestMap.get(x) - alstMap.get(x)))).minBy(x => x._2)._1
        val minAestTask = wf.taskById(minAestTaskId).asInstanceOf[DaxTask]
//        val childrenTasks = minAestTask.children
//        val childMinAest = childrenTasks.map(x => (x, math.abs(aestMap.get(x.id) - alstMap.get(x.id)))).minBy(x => x._2)._1
        val firstItem = nodes.map((node) => newSchedule.findTimeSlot(minAestTask, node, context)).minBy(x => x.endTime)
        val minNode = firstItem.node
        scheduleOrder :+= (minAestTask.id, minNode.id)
        scheduledTasks.put(minAestTask.id, minNode.id)
        newSchedule.placeTask(firstItem)
        val idxOfRemove = readyUnsTasks.indexOf(minAestTaskId)
        val idxOfRemove2 = notScheduledTasks.indexOf(minAestTaskId)
        // TODO check remove method

        readyUnsTasks = readyUnsTasks.take(idxOfRemove) ++ readyUnsTasks.drop(idxOfRemove + 1)
        notScheduledTasks = notScheduledTasks.take(idxOfRemove2) ++ notScheduledTasks.drop(idxOfRemove2 + 1)

        // update maps

        // aest
        aestMap = new util.HashMap[String, Double]()
        var readyTasks = tasks.filter(x => x.parents.size == 1 && x.parents.head.isInstanceOf[HeadDaxTask])
        var evaluatedTasks = List[DaxTask]()
        while (readyTasks.nonEmpty) {
          for (t <- readyTasks) {
            var aestNode: CapacityBasedNode = null
            if (scheduledTasks.containsKey(t.id)) {
              aestNode = environment.nodeById(scheduledTasks.get(t.id)).asInstanceOf[CapacityBasedNode]
            } else {
              aestNode = nodes(rnd.nextInt(nodes.size))
            }
            val aestVal = aest(t, aestNode)
            evaluatedTasks :+= t
            aestMap.put(t.id, aestVal)
          }
          // TODO check headTasks
          readyTasks = List[DaxTask]()
          for (t <- tasks) {
            var isReady = true
            if (evaluatedTasks.contains(t)) {
              isReady = false
            } else {
              val tparents = t.parents
              for (par <- tparents) {
                if (!readyTasks.contains(par)) {
                  isReady = false
                }
              }
            }
            if (isReady) {
              readyTasks :+= t
            }
          }
        }

        // dcpl
        dcplVal = dcpl()

        // alst
        alstMap = new util.HashMap[String, Double]()
        var alstReadyTasks = tasks.filter(x => x.children.isEmpty)
        var alstEvaluatedTasks = List[DaxTask]()
        while (alstReadyTasks.nonEmpty) {
          for (t <- alstReadyTasks) {
            var alstNode: CapacityBasedNode = null
            if (scheduledTasks.containsKey(t.id)) {
              alstNode = environment.nodeById(scheduledTasks.get(t.id)).asInstanceOf[CapacityBasedNode]
            } else {
              alstNode = nodes(rnd.nextInt(nodes.size))
            }
            val alstVal = alst(t, alstNode)
            alstEvaluatedTasks :+= t
            aestMap.put(t.id, alstVal)
          }
          // TODO check headTasks
          alstReadyTasks = List[DaxTask]()
          for (t <- tasks) {
            var isReady = true
            if (alstEvaluatedTasks.contains(t)) {
              isReady = false
            } else {
              val tchildren = t.children
              for (child <- tchildren) {
                if (!alstReadyTasks.contains(child)) {
                  isReady = false
                }
              }
            }
            if (isReady) {
              alstReadyTasks :+= t
            }
          }
        }
      }

    }

    newSchedule
  }
}
