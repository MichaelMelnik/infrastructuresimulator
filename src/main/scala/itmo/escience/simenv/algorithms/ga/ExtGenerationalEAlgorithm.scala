package itmo.escience.simenv.algorithms.ga

import java.io.PrintWriter
import java.util
import java.util.Random

import itmo.escience.simenv.environment.entities.{Node, Task}
import itmo.escience.simenv.utilities.Utilities
import org.uncommons.watchmaker.framework._
import scala.collection.JavaConversions._

/**
  * Created by mikhail on 02.02.2016.
  */
class ExtGenerationalEAlgorithm[T <: Task, N <: Node](factory: ScheduleCandidateFactory[T, N],
                                pipeline: EvolutionaryOperator[WFSchedSolution],
                                fitnessEvaluator: FitnessEvaluator[WFSchedSolution],
                                selector: SelectionStrategy[Object],
                                rng: Random, popSize: Int, expId: String="") extends GenerationalEvolutionEngine[WFSchedSolution](factory,
                                                                           pipeline,
                                                                           fitnessEvaluator,
                                                                           selector,
                                                                           rng) {
  def evolve1(populationSize: Int, eliteCount: Int, seedCandidates: util.Collection[WFSchedSolution], conditions: TerminationCondition): WFSchedSolution = {
    evolvePopulation1(populationSize, eliteCount, seedCandidates, conditions).get(0).getCandidate
  }

  def evolveDetailed(populationSize: Int, eliteCount: Int, seedCandidates: util.Collection[WFSchedSolution], conditions: TerminationCondition): (WFSchedSolution, List[Double]) = {
    val res = evolvePopulationDetailed(populationSize, eliteCount, seedCandidates, conditions)
    (res._1.get(0).getCandidate, res._2)
  }

  def evolvePopulation1(populationSize: Int, eliteCount: Int, seedCandidates: util.Collection[WFSchedSolution], conditions: TerminationCondition): util.List[EvaluatedCandidate[WFSchedSolution]] = {
    if(eliteCount >= 0 && eliteCount < populationSize) {
      if(conditions == null) {
        throw new IllegalArgumentException("At least one TerminationCondition must be specified.")
      } else {
        //        this.satisfiedTerminationConditions = null
        var currentGenerationIndex: Int = 0
        val startTime: Long = System.currentTimeMillis()
        val population = factory.generateInitialPopulation(populationSize, seedCandidates, rng)
        var evaluatedPopulation = evaluate(population)
        EvolutionUtils.sortEvaluatedPopulation(evaluatedPopulation, fitnessEvaluator.isNatural)
        var data: PopulationData[WFSchedSolution] = EvolutionUtils.getPopulationData(evaluatedPopulation, fitnessEvaluator.isNatural, eliteCount, currentGenerationIndex, startTime)
        val best = data.getBestCandidate
//        println(s"Generation ${data.getGenerationNumber}: ${best.fitness}")

        val file = new PrintWriter("D:\\Papers\\DenisDef\\exps\\data\\" + expId + "_ga" + ".csv", "UTF-8")
        val timefile = new PrintWriter("D:\\Papers\\DenisDef\\exps\\algs_times\\" + expId + "_ga_time" + ".csv", "UTF-8")
        while(!conditions.shouldTerminate(data)) {
          currentGenerationIndex += 1
          val starts_time = System.currentTimeMillis()
          evaluatedPopulation = nextEvolutionStep(evaluatedPopulation, eliteCount, rng)
          data = EvolutionUtils.getPopulationData(evaluatedPopulation, fitnessEvaluator.isNatural, eliteCount, currentGenerationIndex, startTime)
          val fin_time = System.currentTimeMillis()
          timefile.write((fin_time - starts_time) + "\n")
          val best = data.getBestCandidate
          file.write(s"${data.getGenerationNumber}\t${best.fitness}\n")
//          println(s"Generation ${data.getGenerationNumber}: ${best.fitness}")
        }
        file.close()
        timefile.close()
        EvolutionUtils.sortEvaluatedPopulation(evaluatedPopulation, fitnessEvaluator.isNatural)
        evaluatedPopulation
      }
    } else {
      throw new IllegalArgumentException("Elite count must be non-negative and less than population size.")
    }
  }

  private var _fitnesses = 0
  def evolvePopulationDetailed(populationSize: Int, eliteCount: Int, seedCandidates: util.Collection[WFSchedSolution], conditions: TerminationCondition): (util.List[EvaluatedCandidate[WFSchedSolution]], List[Double]) = {
    _fitnesses = 0
    if(eliteCount >= 0 && eliteCount < populationSize) {
      if(conditions == null) {
        throw new IllegalArgumentException("At least one TerminationCondition must be specified.")
      } else {
        var currentGenerationIndex: Int = 0
        val startTime: Long = System.currentTimeMillis()
        val population = factory.generateInitialPopulation(populationSize, seedCandidates, rng)
        var evaluatedPopulation = evaluate(population)
        EvolutionUtils.sortEvaluatedPopulation(evaluatedPopulation, fitnessEvaluator.isNatural)
        var data: PopulationData[WFSchedSolution] = EvolutionUtils.getPopulationData(evaluatedPopulation, fitnessEvaluator.isNatural, eliteCount, currentGenerationIndex, startTime)
        val best = data.getBestCandidate

        var dataLog = List[List[Double]]()

        var timePing: Long = System.currentTimeMillis()
        dataLog :+= List[Double](best.fitness, data.getGenerationNumber, timePing - startTime, _fitnesses)
//        println(s"Generation ${data.getGenerationNumber}: ${best.fitness}")


        while(!conditions.shouldTerminate(data)) {
          currentGenerationIndex += 1
          evaluatedPopulation = nextEvolutionStep(evaluatedPopulation, eliteCount, rng)
          data = EvolutionUtils.getPopulationData(evaluatedPopulation, fitnessEvaluator.isNatural, eliteCount, currentGenerationIndex, startTime)

          val best = data.getBestCandidate

          timePing = System.currentTimeMillis()
          dataLog :+= List[Double](best.fitness, data.getGenerationNumber, timePing - startTime, _fitnesses)
//          println(s"Generation ${data.getGenerationNumber}: ${best.fitness}")


        }

        val details = getDetails(dataLog)
        EvolutionUtils.sortEvaluatedPopulation(evaluatedPopulation, fitnessEvaluator.isNatural)
        (evaluatedPopulation, details)
      }
    } else {
      throw new IllegalArgumentException("Elite count must be non-negative and less than population size.")
    }
  }

  def getDetails(dataLog: List[List[Double]]): List[Double] = {
    val len = dataLog.size
    val resultLog = dataLog(len - 1)
    val resultMakespan = resultLog(0)
    val convergenceCriteria = resultMakespan + resultMakespan * 0.001
    var convergenceIter = len - 1
    var curMakespan = resultMakespan
    while (curMakespan < convergenceCriteria && convergenceIter > 0) {
      convergenceIter -= 1
      curMakespan = dataLog(convergenceIter)(0)
    }
    val convData = dataLog(convergenceIter) // makespan iteration time
    List[Double](resultMakespan, convData(0), convData(1), resultLog(2), convData(2), resultLog(3), convData(3))
  }

  override def nextEvolutionStep(evaluatedPopulation: util.List[EvaluatedCandidate[WFSchedSolution]], eliteCount: Int, rng: Random): util.List[EvaluatedCandidate[WFSchedSolution]] = {
    val population :util.ArrayList[WFSchedSolution] = new util.ArrayList[WFSchedSolution](evaluatedPopulation.size())
    val elite: util.ArrayList[WFSchedSolution] = new util.ArrayList[WFSchedSolution](eliteCount)
    EvolutionUtils.sortEvaluatedPopulation(evaluatedPopulation, fitnessEvaluator.isNatural)
    val iterator: util.Iterator[EvaluatedCandidate[WFSchedSolution]] = evaluatedPopulation.iterator()

    val nextEvaluatedPopulation: util.List[EvaluatedCandidate[WFSchedSolution]] = new  util.ArrayList[EvaluatedCandidate[WFSchedSolution]]()

    while(elite.size() < eliteCount) {
      val item = iterator.next
      elite.add(item.getCandidate.copy)
      nextEvaluatedPopulation.add(item)
    }

    while (iterator.hasNext) {
      nextEvaluatedPopulation.add(iterator.next)
    }

    population.addAll(selector.select(nextEvaluatedPopulation, fitnessEvaluator.isNatural, popSize - eliteCount, rng))
    val population1: util.List[WFSchedSolution] = pipeline.apply(population, rng)
    val evPop = evaluate(population1)
    evPop.addAll(elite.map(x => new EvaluatedCandidate[WFSchedSolution](x, x.fitness)))
    EvolutionUtils.sortEvaluatedPopulation(evPop, fitnessEvaluator.isNatural)
    evPop
  }

  def evaluate(pop: util.List[WFSchedSolution]): util.List[EvaluatedCandidate[WFSchedSolution]] = {
    _fitnesses += pop.size()
    val res = new util.ArrayList[EvaluatedCandidate[WFSchedSolution]]()
    val parallel = pop.par.map(p => {
        val fit = fitnessEvaluator.getFitness(p, null)
        p.fitness = fit
        new EvaluatedCandidate[WFSchedSolution](p, fit)
      }
    )
    for (p <- parallel.toList) {
      res.add(p)
    }
//    for (p <- pop) {
//      val fit = fitnessEvaluator.getFitness(p, null)
//      p.fitness = fit
//      res.add(new EvaluatedCandidate[WFSchedSolution](p, fit))
//    }
    res
  }
}
