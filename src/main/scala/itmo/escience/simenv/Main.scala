package itmo.escience.simenv

import itmo.escience.simenv.experiments._
import itmo.escience.simenv.utilities.Units._



/** This is the enter point into the simulator.
  */
object Main {
  def main(args: Array[String]): Unit = {
    val start = System.currentTimeMillis()
    val finish = System.currentTimeMillis()
    gaHeftExps()
    println("Time = " + (finish - start))


  }

  def gaHeftExps() = {
//    val wfPath: String = ".\\resources\\wf-examples\\Montage_25"
    val wfPath: String = ".\\resources\\wf-examples\\Montage_50"
//    val wfPath: String = ".\\resources\\wf-examples\\Montage_100"
    val envArray: List[List[Double]] = List(List(10, 15, 25, 30))
//    val envArray: List[List[Double]] = List(List(10, 15, 25, 30), List(10, 15, 25, 30))
//    val envArray: List[List[Double]] = List(List(20, 23), List(25, 15))
    val globNet: Double = 10 Mbit_Sec
    val locNet: Double = 100 Mbit_Sec
    val reliability: Double = 1.0

    val gaExp = new GAStaticExp(wfPath, envArray, globNet, locNet, reliability)
    val gaMakespan = gaExp.run()
    println("GA makespan = " + gaMakespan)

    val heftExp = new HEFTStaticExp(wfPath, envArray, globNet, locNet, reliability)
    val heftMakespan = heftExp.run()
    println("HEFT makespan = " + heftMakespan)
  }

}
