package itmo.escience.simenv.experiments

import itmo.escience.simenv.environment.entities.{Node, DaxTask}


trait Experiment extends Serializable {
  def run(): Double
}
