package itmo.escience.simenv.environment.entitiesimpl

import itmo.escience.simenv.environment.entities.{CapacityBasedNode, Node, Network, DaxTask}
import itmo.escience.simenv.environment.modelling.{Environment, Estimator}

/**
 * Created by Nikolay on 11/29/2015.
 */
class BasicEstimator[N <: CapacityBasedNode](idealCapacity:Double, env: Environment[N]) extends Estimator[DaxTask, N] with Serializable {

  override def calcTime(task: DaxTask, node: N): Double = {
    // linear
    (idealCapacity / node.capacity) * task.execTime
    // not linear
//  task.execTime / math.log(node.capacity + 1)
  }

  override def calcTransferTime(from: (DaxTask, N), to: (DaxTask, N)): Double = {
    val (parent_task, from_node) = from
    val (child_task, to_node) = to

    // constant
//    if (from_node.id == to_node.id) {
//      return 0.0
//    }
//    if (from_node.parent == to_node.parent) {
//      return 10.0
//    } else
//    {
//    return 100.0
//    }


    // dependent on data
    val from_networks = env.networksByNode(from_node)
    val to_networks = env.networksByNode(to_node)

    val transferNetwork = from_networks.intersect(to_networks).max(new Ordering[Network] {
      override def compare(x: Network, y: Network): Int = x.bandwidth.compare(y.bandwidth)
    })

    val volume = child_task.volumeToTransfer(parent_task)

    //estimate time
    volume / transferNetwork.bandwidth
  }
}
