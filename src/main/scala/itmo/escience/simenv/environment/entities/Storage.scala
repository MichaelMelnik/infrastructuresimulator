package itmo.escience.simenv.environment.entities

import itmo.escience.simenv.common.NameAndId


trait Storage extends NameAndId[StorageId] {

}

object NullStorage extends Storage {
  override def id: StorageId = "NULL_STORAGE"

  override def name: String = "NULL_STORAGE"
}

class SimpleStorage (val id: StorageId, val name: String, val volume: Int, val parent: Storage = NullStorage) extends Storage with Serializable {
  var _files: List[DataFile] = List()

  def addFile(file: DataFile): Unit = {
    //TODO check available space
    _files :+= file
  }

  def files = _files
}
