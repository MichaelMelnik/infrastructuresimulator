package itmo.escience.simenv.environment.entities

import itmo.escience.simenv.common.NameAndId


class DataFile (val id: DataFileId, val name: String, val volume: Double) extends NameAndId[DataFileId] with Serializable
